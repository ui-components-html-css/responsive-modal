# Responsive Modal

##### index.html and style.css
Basic modal with just content (not responsive)

##### index2.html and style2.css
Responsive modal with header, body and footer

##### main.js
Contains JS code for modal open/close functionality
