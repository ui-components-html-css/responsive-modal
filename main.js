// get the modal element
const modal = document.getElementById('simpleModal');

// get open modal button
const modalBtn = document.getElementById('modalBtn');

// get close modal button
const closeBtn = document.getElementsByClassName('closeBtn')[0];

// listen for click on modal open button
modalBtn.addEventListener('click', openModal);

function openModal() {
    modal.style.display = 'block';
};

// listen for click on close modal button
closeBtn.addEventListener('click', closeModal);

function closeModal() {
    modal.style.display = 'none';
};

// listen for outside click
window.addEventListener('click', outsideClick);

function outsideClick(e) {
    if (e.target === modal) {
        modal.style.display = 'none';
    }
}